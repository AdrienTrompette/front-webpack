<?php

namespace App\Repository;

use App\Entity\StudentNumber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StudentNumber|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentNumber|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentNumber[]    findAll()
 * @method StudentNumber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentNumberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudentNumber::class);
    }

    // /**
    //  * @return StudentNumber[] Returns an array of StudentNumber objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StudentNumber
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
