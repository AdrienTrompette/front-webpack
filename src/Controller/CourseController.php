<?php

namespace App\Controller;

use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends AbstractController
{
    #[Route('/courses', name: 'courses')]
    public function courses(CourseRepository $repository): Response
    {
        return $this->render('pages/courses.html.twig', [
            'courses' => $repository->findAll()
        ]);
    }
}
