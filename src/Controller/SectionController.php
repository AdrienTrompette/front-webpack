<?php

namespace App\Controller;

use App\Repository\CourseCategoryRepository;
use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SectionController extends AbstractController
{
    #[Route('/sections', name: 'sections')]
    public function index(CourseRepository $courseRepository, CourseCategoryRepository $courseCategoryRepository): Response
    {
        return $this->render('pages/sections.html.twig', [
            'courses' => $courseRepository->findAll(),
            'categories' => $courseCategoryRepository->findAll()
        ]);
    }
}
