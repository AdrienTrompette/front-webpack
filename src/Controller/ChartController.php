<?php

namespace App\Controller;

use App\Repository\StudentNumberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class ChartController extends AbstractController
{
    #[Route('/chart', name: 'chart')]
    public function chartjs(ChartBuilderInterface $chartBuilder): Response
    {
        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'labels' => ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            'datasets' => [
                [
                    'label' => 'Evolution des ventes',
                    'borderColor' => '#aaa',
                    'data' => [0, 10, 5, 70, 20, 30, 45],
                ],
            ],
        ]);

        $chart->setOptions([/* ... */]);

        return $this->render('chart/chartdemo.html.twig', [
            'chart' => $chart,
        ]);
    }

    #[Route('/chartdb', name: 'chartdb')]
    public function chartdb(ChartBuilderInterface $chartBuilder, StudentNumberRepository $studentNumberRepository)
    {
        $chart = $chartBuilder->createChart(Chart::TYPE_DOUGHNUT);
        $labels = [];
        $data = [];
        $students = $studentNumberRepository->findAll();
        foreach($students as $student) {
            $labels[] = $student->getYear();
            $data[] = $student->getNumber();
        }
        $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Evolution des ventes',
                    'backgroundColor' => ['#E2C2B9', '#F2DDC1', '#99A799', '#D3E4CD', '#F90716', '#FFF323'],
                    'borderColor' => '#aaa',
                    'data' => $data,
                ],
            ],
        ]);

        $chart->setOptions([/* ... */]);

        return $this->render('chart/chartdb.html.twig', [
            'chart' => $chart,
        ]);
    }
}