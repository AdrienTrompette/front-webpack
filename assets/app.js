/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss'

// Bootstrap JS
import 'bootstrap'

// Test JS
console.log('Webpack')

// Jquery
import 'jquery'
const $ = require('jquery') // utilisation de la variable $ dans Webpack
global.$ = global.jQuery = $ // Rendre globale la variable $ pour l'utilisation hors Webpack

$(document).ready(function() {
    $("p").click(function(){
        $(this).hide();
    });
});

// DataTables
import 'datatables.net-bs5'

$(document).ready(function() {
    $('#table-admin').DataTable(
        {
            'info': false,
            'language': {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
            }
        }
    );
} );

// Filterizr
import Filterizr from 'filterizr'

const options = { /* check next step for available options */ }

const filterizr = new Filterizr('.filter-container', options)

// start the Stimulus application
import './bootstrap'
